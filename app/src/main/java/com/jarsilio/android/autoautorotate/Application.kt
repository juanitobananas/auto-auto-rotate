package com.jarsilio.android.autoautorotate

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import com.jarsilio.android.autoautorotate.prefs.Prefs
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import timber.log.Timber

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()

        appContext = applicationContext

        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))

        AppCompatDelegate.setDefaultNightMode(Prefs.dayNightMode)
    }
}

private var appContext: Context? = null

fun requireApplicationContext(): Context {
    return appContext ?: throw IllegalStateException(
        "ApplicationContext should not be null. Did you forget to set it in your Application's onCreate()?",
    )
}
