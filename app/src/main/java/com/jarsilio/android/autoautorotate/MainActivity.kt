package com.jarsilio.android.autoautorotate

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.jarsilio.android.autoautorotate.appintro.AppIntro
import com.jarsilio.android.autoautorotate.applist.AppsHandler
import com.jarsilio.android.autoautorotate.extensions.isAccessibilityServiceEnabled
import com.jarsilio.android.autoautorotate.extensions.isIgnoringBatteryOptimizations
import com.jarsilio.android.autoautorotate.extensions.isUsageAccessAllowed
import com.jarsilio.android.autoautorotate.extensions.isWriteSettingsAllowed
import com.jarsilio.android.autoautorotate.prefs.Prefs
import com.jarsilio.android.autoautorotate.services.PersistentService
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.common.menu.CommonMenu
import com.jarsilio.android.common.privacypolicy.PrivacyPolicyBuilder
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.LibsBuilder
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    private val prefs = Prefs
    private val commonMenu: CommonMenu by lazy { CommonMenu(this) }

    private lateinit var appIntroActivityResultLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportFragmentManager.beginTransaction().replace(R.id.settings, SettingsFragment()).commit()

        AppsHandler.updateAppsDatabase()

        registerActivityResultLaunchers()
    }

    private fun registerActivityResultLaunchers() {
        appIntroActivityResultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
                Timber.d(
                    "Returned from activity: requestCode ${activityResult.resultCode}. " +
                        "resultCode: ${activityResult.resultCode} (Activity.RESULT_OK = ${Activity.RESULT_OK})",
                )
                if (activityResult.resultCode != Activity.RESULT_OK) {
                    Timber.d(
                        "AppIntro didn't exit correctly (resultCode != Activity.RESULT_OK). " +
                            "Probably user went back. Closing MainActivity...",
                    )
                    finish()
                }
            }
    }

    override fun onStart() {
        super.onStart()

        if (!(isUsageAccessAllowed || isAccessibilityServiceEnabled) || !isWriteSettingsAllowed) {
            Timber.e(
                "'Usage access' or 'Accessibility Service' and 'Write settings' permission " +
                    "must be allowed. Disabling Auto Auto-Rotate and starting AppIntro",
            )

            prefs.isEnabled = false
            PersistentService.stopService(this)

            appIntroActivityResultLauncher.launch(Intent(this, AppIntro::class.java))
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)

        if (hasFocus) {
            prefs.useAccessibilityService = isAccessibilityServiceEnabled
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        commonMenu.addImpressumToMenu(menu)
        // commonMenu.addSendDebugLogsToMenu(menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_privacy_policy -> showPrivacyPolicyActivity()
            R.id.menu_item_licenses -> showAboutLicensesActivity()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        invalidateOptionsMenu()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            delegate.localNightMode = prefs.dayNightMode
        }
    }

    private fun showPrivacyPolicyActivity() {
        val privacyPolicyBuilder =
            PrivacyPolicyBuilder()
                .withIntro(getString(R.string.app_name), "Juan García Basilio (juanitobananas)")
                .withUrl("https://gitlab.com/juanitobananas/auto-auto-rotate/blob/master/PRIVACY.md#auto-auto-rotate-privacy-policy")
                .withMeSection()
                .withEmailSection("juam+autoautorotate@posteo.net")
                .withAutoGoogleOrFDroidSection()
        privacyPolicyBuilder.start(this)
    }

    private fun showAboutLicensesActivity() {
        var style = Libs.ActivityStyle.LIGHT_DARK_TOOLBAR
        var theme = R.style.AppTheme_About_Light

        val currentNightMode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            style = Libs.ActivityStyle.DARK
            theme = R.style.AppTheme_About_Dark
        }

        LibsBuilder()
            .withActivityStyle(style)
            .withActivityTheme(theme)
            .withAboutIconShown(true)
            .withAboutVersionShown(true)
            .withActivityTitle(getString(R.string.menu_item_licenses))
            .withAboutDescription(getString(R.string.licenses_about_libraries_text))
            .start(applicationContext)
    }

    class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {
        private val prefs = Prefs
        private val progressBar: ProgressBar by lazy { requireActivity().findViewById(R.id.progressBar) }
        private val settingsLayout: FrameLayout by lazy { requireActivity().findViewById(R.id.settings) }

        private lateinit var batteryOptimizationSettingsActivityResultLauncher: ActivityResultLauncher<Intent>
        private lateinit var accessibilityServicesSettingsActivityResultLauncher: ActivityResultLauncher<Intent>

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            registerActivityResultLaunchers()
        }

        private fun registerActivityResultLaunchers() {
            batteryOptimizationSettingsActivityResultLauncher =
                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                    if (!requireContext().isIgnoringBatteryOptimizations) {
                        Timber.d("The user didn't accept the ignoring of the battery optimization. Forcing show_notification to true")
                        prefs.showNotification = true
                    } else {
                        PersistentService.restartService(requireContext())
                    }
                }
            accessibilityServicesSettingsActivityResultLauncher =
                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                    // prefs.useAccessibilityService = requireContext().isAccessibilityServiceEnabled // We don't really need to do this here, as we already do it onWindowFocused
                    PersistentService.restartService(requireContext())
                }
        }

        override fun onCreatePreferences(
            savedInstanceState: Bundle?,
            rootKey: String?,
        ) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            populateDayNightListPreference()
        }

        private fun populateDayNightListPreference() {
            val dayNightPreferences = findPreference<ListPreference>(Prefs.DAY_NIGHT_MODE)!!
            dayNightPreferences.entries =
                arrayOf(
                    getString(R.string.settings_string_array_day),
                    getString(R.string.settings_string_array_night),
                )
            dayNightPreferences.entryValues =
                arrayOf(
                    AppCompatDelegate.MODE_NIGHT_NO.toString(),
                    AppCompatDelegate.MODE_NIGHT_YES.toString(),
                )
            dayNightPreferences.setDefaultValue(AppCompatDelegate.MODE_NIGHT_NO.toString())

            if (isPieOrNewer) {
                dayNightPreferences.entries = dayNightPreferences.entries.plus(getString(R.string.settings_string_array_follow_system))
                dayNightPreferences.entryValues =
                    dayNightPreferences.entryValues.plus(
                        AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM.toString(),
                    )
                dayNightPreferences.setDefaultValue(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM.toString())
            }
        }

        override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?,
        ) {
            super.onViewCreated(view, savedInstanceState)

            val appListPreference = findPreference<Preference>(prefs.APP_LIST)
            appListPreference?.setOnPreferenceClickListener {
                // This progress bar is hidden onResume(). I would've liked to add this to AppListActivity while the RecyclerView was filling in, but I just couldn't manage. This seems to do the trick pretty well.
                progressBar.visibility = View.VISIBLE
                settingsLayout.visibility = View.INVISIBLE
                startActivity(Intent(activity, AppListActivity::class.java))
                true
            }

            // Classically, I've opened these Activities from the onSharedPreferenceChanged method.
            // In this case, I want the UI to change depending on the accessibility services.
            // For that, I change the preference onWindowFocusChanged. This would trigger an onSharedPreferenceChanged again, re-opening the settings activity... Like this, we only launch the activity once *and* get to change the UI (calling onSharedPreference every time, but we don't care as we don't do anything) onWindowsFocusChanged
            val useAccessibilityServicePreference = findPreference<Preference>(prefs.USE_ACCESSIBILITY_SERVICE)
            useAccessibilityServicePreference?.setOnPreferenceClickListener {
                if (prefs.useAccessibilityService) {
                    Timber.d("Showing dialog to see if the user wants to open Android's 'Accessibility Settings'")
                    AlertDialog.Builder(requireContext()).apply {
                        setMessage(R.string.accessibility_service_explanation)
                        setPositiveButton(android.R.string.ok) { _, _ ->
                            Timber.d("Opening Android's 'Accessibility Settings' activity")
                            accessibilityServicesSettingsActivityResultLauncher.launch((Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)))
                        }
                        setNegativeButton(android.R.string.cancel) { _, _ ->
                            Timber.d("User decided *not* to open 'Accessibility Settings' activity")
                        }
                        setCancelable(false)
                        show()
                    }
                } else {
                    // The user is switching it off. Do not show explanation dialog
                    Timber.d("Not showing dialog, as user is turning accessibility services off")
                    accessibilityServicesSettingsActivityResultLauncher.launch((Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS)))
                }
                true
            }
        }

        override fun onStart() {
            super.onStart()
            prefs.settingsFragment = this
        }

        override fun onResume() {
            super.onResume()
            prefs.prefs.registerOnSharedPreferenceChangeListener(this)
            settingsLayout.visibility = View.VISIBLE
            progressBar.visibility = View.INVISIBLE
        }

        override fun onPause() {
            super.onPause()
            prefs.prefs.unregisterOnSharedPreferenceChangeListener(this)
        }

        override fun onDestroy() {
            super.onDestroy()
            prefs.settingsFragment = null
        }

        override fun onSharedPreferenceChanged(
            sharedPreferences: SharedPreferences?,
            key: String?,
        ) {
            Timber.d("Changed preference: $key")
            when (key) {
                prefs.IS_ENABLED -> {
                    if (prefs.isEnabled) {
                        PersistentService.startService(requireContext())
                    } else {
                        PersistentService.stopService(requireContext())
                    }
                }
                prefs.SHOW_NOTIFICATION -> {
                    if (!prefs.showNotification && !requireContext().isIgnoringBatteryOptimizations) {
                        Timber.d("Requesting to ignore battery optimizations")
                        batteryOptimizationSettingsActivityResultLauncher.launch(
                            Intent(
                                Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                                Uri.parse("package:${requireContext().packageName}"),
                            ),
                        )
                    } else {
                        PersistentService.restartService(requireContext())
                    }
                }
                prefs.DAY_NIGHT_MODE -> {
                    Timber.d("Changing theme to ${prefs.dayNightMode}")
                    setNightMode(prefs.dayNightMode)
                }
            }
        }

        private fun setNightMode(nightMode: Int) {
            AppCompatDelegate.setDefaultNightMode(nightMode)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
                (requireActivity() as AppCompatActivity).delegate.localNightMode = nightMode
            }
        }

        private fun openAccessibilitySettingsExplanationIfNecessary() {
            Timber.d("Showing dialog to see if the user wants to open Android's 'Accessibility Settings'")
            AlertDialog.Builder(requireContext()).apply {
                setMessage(R.string.accessibility_service_explanation)
                setPositiveButton(android.R.string.ok) { _, _ ->
                    Timber.d("Opening Android's 'Accessibility Settings' activity")
                }
                setNegativeButton(android.R.string.cancel) { _, _ ->
                    Timber.d("User decided *not* to open 'Accessibility Settings' activity")
                }
                setCancelable(false)
                show()
            }
        }
    }
}
