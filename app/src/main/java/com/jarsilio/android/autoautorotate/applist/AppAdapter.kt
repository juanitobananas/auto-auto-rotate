package com.jarsilio.android.autoautorotate.applist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.jarsilio.android.autoautorotate.R
import timber.log.Timber

class AppListAdapter : ListAdapter<App, AppHolder>(AppDiffCallback()) {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): AppHolder {
        val appCardView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.app_card, parent, false) as CardView
        return AppHolder(appCardView)
    }

    override fun onBindViewHolder(
        holder: AppHolder,
        position: Int,
    ) {
        holder.updateWithApp(getItem(position))
    }
}

class AppHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val icon = view.findViewById<View>(R.id.app_icon) as ImageView
    private val appName = view.findViewById<View>(R.id.app_name) as TextView
    private val packageName = view.findViewById<View>(R.id.package_name) as TextView
    private val cardView = view.findViewById<View>(R.id.card_view) as CardView
    private val dao = AppDatabase.getInstance(view.context).appsDao()

    fun updateWithApp(app: App) {
        icon.setImageDrawable(app.getIcon(cardView.context))
        appName.text = app.name
        packageName.text = app.packageName

        if (app.isSystem) {
            packageName.setTextColor(ContextCompat.getColor(cardView.context, R.color.darkBlue))
        }

        // Adding click listener on CardView to open clicked application directly from here .
        cardView.setOnClickListener {
            Timber.d("Clicked on ${app.packageName}.")
            toggleIsAutorotate(cardView, app)
        }
    }

    private fun toggleIsAutorotate(
        view: View,
        app: App,
    ) {
        Thread {
            dao.setAutorotate(app.packageName, !app.isAutorotate)
        }.start()

        val snackBarMessage: String
        val debugMessage: String
        val undoDebugMessage: String

        if (!app.isAutorotate) {
            debugMessage = "Added ${app.packageName} to auto-rotate list"
            snackBarMessage = view.context.getString(R.string.added_app, app.name)
            undoDebugMessage = "Undid adding ${app.packageName} to auto-rotate list"
        } else {
            debugMessage = "Removed ${app.packageName} from auto-rotate list"
            snackBarMessage = view.context.getString(R.string.removed_app, app.name)
            undoDebugMessage = "Undid removing ${app.packageName} from auto-rotate list"
        }

        Timber.d(debugMessage)

        Snackbar.make(view, snackBarMessage, Snackbar.LENGTH_LONG)
            .setAction(R.string.undo) {
                Timber.d(undoDebugMessage)
                Thread {
                    dao.setAutorotate(app.packageName, app.isAutorotate)
                }.start()
            }.show()
    }
}

class AppDiffCallback : DiffUtil.ItemCallback<App>() {
    override fun areItemsTheSame(
        oldItem: App,
        newItem: App,
    ): Boolean {
        return oldItem.packageName == newItem.packageName
    }

    override fun areContentsTheSame(
        oldItem: App,
        newItem: App,
    ): Boolean {
        return oldItem == newItem
    }
}
