package com.jarsilio.android.autoautorotate.extensions

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ActivityManager
import android.app.AppOpsManager
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import androidx.core.content.ContextCompat
import com.jarsilio.android.autoautorotate.services.AppLaunchDetectionService
import com.jarsilio.android.common.extensions.isLollipopOrNewer
import com.jarsilio.android.common.extensions.isQOrNewer
import com.jarsilio.android.common.extensions.isTiramisuOrNewer
import timber.log.Timber

var lastForegroundApp: String = ""

val Context.foregroundApp: String
    get() {
        if (isAccessibilityServiceEnabled) {
            return lastForegroundApp
        }

        // Use "Usage Access" if AccessibilityService not enabled
        var foregroundApp = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            @SuppressLint("WrongConstant")
            val usageStatsManager = getSystemService("usagestats") as UsageStatsManager?
            if (usageStatsManager == null) {
                Timber.e("Couldn't determine currently running app (failed to get UsageStatsManager).")
            } else {
                val now = System.currentTimeMillis()
                val usageStatsList = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, now - 1000 * 1000, now)
                var lastTimeUsed: Long = 0
                for (usageStats in usageStatsList) {
                    if (usageStats.lastTimeUsed > lastTimeUsed) {
                        lastTimeUsed = usageStats.lastTimeUsed
                        if (usageStats.packageName != "android") {
                            // I don't know what this is exactly, but sometimes this appid gets in the way.
                            foregroundApp = usageStats.packageName
                        } else {
                            Timber.d("Couldn't determine currently running app (packagename 'android').")
                        }
                    }
                }
            }
        } else {
            val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val tasks = activityManager.runningAppProcesses
            if (tasks != null && tasks[0] != null) {
                if (tasks[0].processName != "android") {
                    foregroundApp = tasks[0].processName
                } else {
                    Timber.d("Couldn't determine currently running app (packagename 'android').")
                }
            } else {
                Timber.e("Couldn't determine currently running app (using ActivityManager).")
            }
        }

        if (foregroundApp == "") {
            Timber.d("Assuming it's still the last one: $lastForegroundApp")
            foregroundApp = lastForegroundApp
        }

        lastForegroundApp = foregroundApp

        return foregroundApp
    }

val Context.isScreenOn: Boolean
    get() {
        val powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            powerManager.isInteractive
        } else {
            powerManager.isScreenOn
        }
    }

val Context.isUsageAccessAllowed: Boolean

    @TargetApi(Build.VERSION_CODES.Q)
    get() {
        return if (isLollipopOrNewer) {
            try {
                val packageManager = applicationContext.packageManager
                val applicationInfo = packageManager.getApplicationInfo(applicationContext.packageName, 0)
                val appOpsManager = applicationContext.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
                val mode =
                    if (isQOrNewer) {
                        appOpsManager.unsafeCheckOpNoThrow(
                            AppOpsManager.OPSTR_GET_USAGE_STATS,
                            applicationInfo.uid,
                            applicationInfo.packageName,
                        )
                    } else {
                        appOpsManager.checkOpNoThrow(
                            AppOpsManager.OPSTR_GET_USAGE_STATS,
                            applicationInfo.uid,
                            applicationInfo.packageName,
                        )
                    }
                mode == AppOpsManager.MODE_ALLOWED
            } catch (e: PackageManager.NameNotFoundException) {
                false
            }
        } else {
            true
        }
    }

val Context.isWriteSettingsAllowed: Boolean
    get() = Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.System.canWrite(applicationContext)

val Context.isIgnoringBatteryOptimizations: Boolean
    get() {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val powerManager = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
            powerManager.isIgnoringBatteryOptimizations(applicationContext.packageName)
        } else {
            true
        }
    }

val Context.isPostNotificationsPermissionGranted: Boolean
    get() {
        return if (isTiramisuOrNewer) {
            val permissionCheck = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.POST_NOTIFICATIONS)
            permissionCheck == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
    }

val Context.isAccessibilityServiceEnabled: Boolean
    get() {
        val expectedLockAccessibilityService = "$packageName/${AppLaunchDetectionService::class.java.canonicalName}"

        val enabledAccessibilityServices: String? =
            Settings.Secure.getString(
                contentResolver,
                Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES,
            )

        if (enabledAccessibilityServices != null) {
            for (accessibilityService in enabledAccessibilityServices.split(":")) {
                if (accessibilityService.equals(expectedLockAccessibilityService, ignoreCase = true)) {
                    return true
                }
            }
        }
        return false
    }

val Context.isInstalledViaGooglePlay: Boolean
    @TargetApi(Build.VERSION_CODES.R)
    get() {
        val installerPackage =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                packageManager.getInstallSourceInfo(packageName).installingPackageName
            } else {
                packageManager.getInstallerPackageName(packageName)
            }

        return installerPackage == "com.android.vending"
    }
