package com.jarsilio.android.autoautorotate.prefs

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.CheckBoxPreference
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreference
import androidx.preference.SwitchPreferenceCompat
import com.jarsilio.android.autoautorotate.MainActivity
import com.jarsilio.android.autoautorotate.R
import com.jarsilio.android.autoautorotate.requireApplicationContext
import com.jarsilio.android.common.extensions.isPieOrNewer

object Prefs {
    private val context = requireApplicationContext()

    val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    var settingsFragment: MainActivity.SettingsFragment? = null

    // User prefs
    val IS_ENABLED: String = context.getString(R.string.pref_enabled_key)
    val SHOW_NOTIFICATION: String = context.getString(R.string.pref_notification_key)
    val APP_LIST: String = context.getString(R.string.pref_app_list_key)
    val USE_ACCESSIBILITY_SERVICE: String = context.getString(R.string.pref_use_accessibility_service_key)
    val DAY_NIGHT_MODE: String = context.getString(R.string.pref_day_night_mode_key)
    val AUTO_SAVE_AUTOROTATE_CONFIG_FOR_APPS: String = context.getString(R.string.pref_auto_save_autorotate_config_for_apps_key)

    var isEnabled: Boolean
        get() = prefs.getBoolean(IS_ENABLED, true)
        set(value) = setBooleanPreference(IS_ENABLED, value)

    var showNotification: Boolean
        get() = prefs.getBoolean(SHOW_NOTIFICATION, true)
        set(value) = setBooleanPreference(SHOW_NOTIFICATION, value)

    var useAccessibilityService: Boolean // Auto Auto-Rotate will actually just check if the accessibility service is enabled and uses it.
        get() = prefs.getBoolean(USE_ACCESSIBILITY_SERVICE, false)
        set(value) = setBooleanPreference(USE_ACCESSIBILITY_SERVICE, value)

    val dayNightMode: Int
        get() {
            val defaultDayNightMode =
                if (isPieOrNewer) {
                    AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM.toString()
                } else {
                    AppCompatDelegate.MODE_NIGHT_NO.toString()
                }
            return prefs.getString(DAY_NIGHT_MODE, defaultDayNightMode)!!.toInt()
        }

    var isAutoSaveAutorotateConfigForApps: Boolean
        get() = prefs.getBoolean(AUTO_SAVE_AUTOROTATE_CONFIG_FOR_APPS, true)
        set(value) = setBooleanPreference(AUTO_SAVE_AUTOROTATE_CONFIG_FOR_APPS, value)

    private fun setBooleanPreference(
        key: String,
        value: Boolean,
    ) {
        // This changes the GUI, but it needs the PreferencesActivity to have started
        when (val preference = settingsFragment?.findPreference(key) as Preference?) {
            is CheckBoxPreference -> preference.isChecked = value
            is SwitchPreference -> preference.isChecked = value
            is SwitchPreferenceCompat -> preference.isChecked = value
        }
        // This doesn't change the GUI
        prefs.edit().putBoolean(key, value).apply()
    }
}
