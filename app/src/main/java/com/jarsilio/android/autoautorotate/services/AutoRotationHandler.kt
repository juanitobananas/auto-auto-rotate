package com.jarsilio.android.autoautorotate.services

import android.database.ContentObserver
import android.provider.Settings
import com.jarsilio.android.autoautorotate.applist.AppDatabase
import com.jarsilio.android.autoautorotate.applist.AppsHandler
import com.jarsilio.android.autoautorotate.extensions.foregroundApp
import com.jarsilio.android.autoautorotate.prefs.Prefs
import com.jarsilio.android.autoautorotate.requireApplicationContext
import timber.log.Timber

object AutoRotationHandler {
    private val context = requireApplicationContext()

    private val autoRotateSettingObserver: AutoRotateSettingObserver by lazy { AutoRotateSettingObserver() }

    fun setAutoRotate(value: Boolean) {
        val currentValue = Settings.System.getInt(context.contentResolver, Settings.System.ACCELEROMETER_ROTATION) == 1
        if (value != currentValue) {
            unregisterAutoRotateSettingObserver()
            Settings.System.putInt(context.contentResolver, Settings.System.ACCELEROMETER_ROTATION, if (value) 1 else 0)
            registerAutoRotateSettingObserverIfNecessary()
        }
    }

    fun registerAutoRotateSettingObserverIfNecessary() {
        if (Prefs.isAutoSaveAutorotateConfigForApps) {
            context.contentResolver.registerContentObserver(
                Settings.System.getUriFor(Settings.System.ACCELEROMETER_ROTATION),
                true,
                autoRotateSettingObserver,
            )
        }
    }

    fun unregisterAutoRotateSettingObserver() {
        context.contentResolver.unregisterContentObserver(autoRotateSettingObserver)
    }

    fun isAppInAutoRotateList(packageName: String): Boolean {
        for (app in AppDatabase.getInstance(context).appsDao().autorotateApps) {
            if (app.packageName == packageName) {
                Timber.d("${app.name} (${app.packageName}) is running in foreground")
                return true
            }
        }
        return false
    }
}

class AutoRotateSettingObserver : ContentObserver(null) {
    private val context = requireApplicationContext()

    private val appsDao = AppDatabase.getInstance(context).appsDao()
    private val appsHandler = AppsHandler

    override fun onChange(selfChange: Boolean) {
        val foregroundApp = context.foregroundApp

        val autoRotate = Settings.System.getInt(context.contentResolver, Settings.System.ACCELEROMETER_ROTATION) == 1
        Timber.d("Auto-Rotate setting changed: $autoRotate")

        if (autoRotate) {
            if (!appsHandler.isAutorotate(foregroundApp)) {
                Timber.d("Auto-rotate set to true for $foregroundApp. Adding to list of apps to auto-rotate")
                appsDao.setAutorotate(foregroundApp, true)
            }
        } else {
            if (appsHandler.isAutorotate(foregroundApp)) {
                Timber.d("Auto-rotate set to false for $foregroundApp. Removing from list of apps to auto-rotate")
                appsDao.setAutorotate(foregroundApp, false)
            }
        }
    }
}
