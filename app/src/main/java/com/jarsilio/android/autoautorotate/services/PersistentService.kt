package com.jarsilio.android.autoautorotate.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.jarsilio.android.autoautorotate.MainActivity
import com.jarsilio.android.autoautorotate.R
import com.jarsilio.android.autoautorotate.extensions.isAccessibilityServiceEnabled
import com.jarsilio.android.autoautorotate.extensions.isIgnoringBatteryOptimizations
import com.jarsilio.android.autoautorotate.extensions.isScreenOn
import com.jarsilio.android.autoautorotate.prefs.Prefs
import timber.log.Timber
import java.lang.IllegalArgumentException

class PersistentService : Service() {
    private val autoRotationHandler = AutoRotationHandler

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun start() {
        if (!isAccessibilityServiceEnabled) {
            // If the AccessibilityService is enabled, it'll start automatically.
            // There is now way to start it or stop it. The ScreenReceiver is only needed for the 'Usage Access' polling version.
            registerScreenReceiver()
        }
        // Only register if option to auto-save auto-rotate option for apps is set to true
        autoRotationHandler.registerAutoRotateSettingObserverIfNecessary()
    }

    private fun stop() {
        unregisterScreenReceiver()
        autoRotationHandler.unregisterAutoRotateSettingObserver()
    }

    override fun onDestroy() {
        stop()
    }

    override fun onCreate() {
        super.onCreate()
        start()
    }

    private fun registerScreenReceiver() {
        val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        registerReceiver(ScreenReceiver, filter)

        if (isScreenOn) {
            // Start directly if screen is on
            ScreenReceiver.scheduleTimer(this)
        }
    }

    private fun unregisterScreenReceiver() {
        ScreenReceiver.unscheduleTimer()
        try {
            unregisterReceiver(ScreenReceiver)
        } catch (e: IllegalArgumentException) {
            Timber.d("Failed to unregister ScreenReceiver (${e.message}). Probably was never registered. This should be okay.")
        }
    }

    override fun onStartCommand(
        intent: Intent?,
        flags: Int,
        startId: Int,
    ): Int {
        if (shouldStartForegroundService(this)) {
            startForegroundService()
        }

        return START_STICKY
    }

    private fun startForegroundService() {
        Timber.d("Starting ForegroundService")

        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val notificationPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE)

        val notificationBuilder =
            NotificationCompat.Builder(this, "persistent")
                .setContentText(getString(R.string.notification_tap_to_open))
                .setShowWhen(false)
                .setContentIntent(notificationPendingIntent)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setSmallIcon(R.drawable.ic_screen_rotation)
                .setOngoing(true)
                .setContentTitle(getString(R.string.notification_service_running))
                .setTicker(getString(R.string.notification_service_running))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel =
                NotificationChannel("persistent", getString(R.string.notification_persistent), NotificationManager.IMPORTANCE_NONE)
            notificationChannel.description = getString(R.string.notification_persistent_channel_description)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }

        startForeground(FOREGROUND_ID, notificationBuilder.build())
    }

    companion object {
        private const val FOREGROUND_ID = 10001

        fun startService(context: Context) {
            val prefs = Prefs
            if (prefs.isEnabled) {
                Timber.i("Starting Auto Auto-Rotate")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                    shouldStartForegroundService(context)
                ) {
                    Timber.d("Starting service with context.startForegroundService (Android >= Oreo and battery optimization on)")
                    context.startForegroundService(Intent(context, PersistentService::class.java))
                } else {
                    Timber.d("Starting service with context.startService (Android < Oreo or battery optimization off)")
                    context.startService(Intent(context, PersistentService::class.java))
                }
            } else {
                Timber.i("Not starting Auto Auto-Rotate because it's disabled")
            }
        }

        fun stopService(context: Context) {
            context.stopService(Intent(context, PersistentService::class.java))
        }

        fun restartService(context: Context) {
            Timber.i("Restarting Auto Auto-Rotate")
            stopService(context)
            startService(context)
        }

        private fun shouldStartForegroundService(context: Context): Boolean {
            return !context.isIgnoringBatteryOptimizations || Prefs.showNotification
        }
    }
}

class AutoStart : BroadcastReceiver() {
    override fun onReceive(
        context: Context,
        intent: Intent,
    ) {
        if (Prefs.isEnabled) {
            if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
                Timber.d("Received ACTION_BOOT_COMPLETED.")
                PersistentService.startService(context)
            } else if (intent.action == Intent.ACTION_MY_PACKAGE_REPLACED) {
                Timber.d("Received ACTION_MY_PACKAGE_REPLACED.")
                PersistentService.startService(context)
            }
        }
    }
}
