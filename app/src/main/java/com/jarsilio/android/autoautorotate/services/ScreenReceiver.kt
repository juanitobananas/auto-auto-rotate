package com.jarsilio.android.autoautorotate.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.jarsilio.android.autoautorotate.extensions.foregroundApp
import com.jarsilio.android.autoautorotate.extensions.isAccessibilityServiceEnabled
import timber.log.Timber
import java.util.Timer
import java.util.TimerTask

object ScreenReceiver : BroadcastReceiver() {
    private var timer: Timer = Timer()

    override fun onReceive(
        context: Context,
        intent: Intent,
    ) {
        if (intent.action == Intent.ACTION_SCREEN_OFF) {
            Timber.d("Screen off. Disabling scheduled task to check running app (TimerTask)")
            unscheduleTimer()
        } else if (intent.action == Intent.ACTION_SCREEN_ON) {
            Timber.d("Screen on. Re-enabling scheduled task to check running app (TimerTask)")
            scheduleTimer(context)
        }
    }

    fun unscheduleTimer() {
        timer.cancel()
    }

    fun scheduleTimer(context: Context) {
        if (context.isAccessibilityServiceEnabled) {
            Timber.d("Not scheduling recurring timer to poll currently running app because the AccessibilityService is enabled.")
            return
        }

        val autoRotationHandler = AutoRotationHandler

        var lastForegroundApp = ""

        timer.cancel()
        timer = Timer()
        timer.schedule(
            object : TimerTask() {
                override fun run() {
                    val foregroundApp = context.foregroundApp

                    if (foregroundApp != lastForegroundApp) { // Only show the log once, and not every second
                        if (autoRotationHandler.isAppInAutoRotateList(foregroundApp)) {
                            Timber.d("$foregroundApp is in the list for auto-rotate. Setting auto-rotate to true (if not already set)")
                            autoRotationHandler.setAutoRotate(true)
                        } else {
                            Timber.d(
                                "$foregroundApp is *not* in the list for auto-rotate. Setting auto-rotate to false (if not already set)",
                            )
                            autoRotationHandler.setAutoRotate(false)
                        }
                    }
                    lastForegroundApp = foregroundApp
                }
            },
            500,
            1000,
        )
    }
}
