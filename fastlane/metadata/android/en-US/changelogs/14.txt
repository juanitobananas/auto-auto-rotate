New in version 0.10.1

★ Small bugfix.

New in version 0.10

★ Make Accessibility Service optional. Some devices
  and Android versions seem to have some issues with it.
