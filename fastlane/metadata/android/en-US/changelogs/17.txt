New in version 0.10.4

★ Small bug fix in app intro.

New in version 0.10.3

★ Add Arabic, Chinese (traditional), Hindi, and
  Portuguese (Brasil). Also machine-translated :shrug:

New in version 0.10.2

★ Add Chinese, French, Russian and Vietnamese translations;
  machine-translated, sorry for that, dear languages! ;)

New in version 0.10.1

★ Small bugfix.

New in version 0.10

★ Make Accessibility Service optional. Some devices
  and Android versions seem to have some issues with it.
