New in version 0.12.4

★ Update Chinese (Simplified) translation
★ Fix Android 14 crash due to missing new foreground service type

New in version 0.12.3

★ Basic maintenance and adaption for newer Androids
★ Ask for 'Post notifications' for Android 13 and above.
★ Upgrade a bunch of dependencies.
★ Remove ACRA for the dev's peace of mind :)
