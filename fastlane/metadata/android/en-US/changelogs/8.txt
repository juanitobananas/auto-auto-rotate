New in version 0.8

★ Work-around to avoid annoying crash during permission setup.

New in version 0.7

★ Reduce APK size.

New in version 0.6

★ Reproducible build: first attempt to make the apk build
  reproducible.

New in version 0.5

★ Add 'Legal notice' and 'Report an issue' menu items.
★ Some small improvements.
