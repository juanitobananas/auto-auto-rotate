<i>Auto-Rotate</i> adalah aplikasi yang benar - benar simpel, berguna untuk menyimpan pengaturan rotasi otomatis setiap aplikasi.

Apakah anda biasanya menonaktifkan rotasi-otomatis dan mengaktifkannya untuk aplikasi khusus? Seperti galeri, atau video? Apakah anda sering lupa untuk menonaktifkannya setelahnya? Apakah itu menggangu anda?

Kalau begitu <i>Auto Auto-Rotate</i> adalah aplikasi yang sempurna untuk Anda!

<i>Auto Auto-Rotate</i> adalah aplikasi bersumber terbuka dan dirilis dibawah lisensi GPLv3[1]. Lihat kodenya jika anda mau[2].

<b>Membutuhkan Perizinan Android</b>

▸ WRITE_SETTINGS untuk menghidupkan dan mematikan rotasi-otomatis Android.
▸ BIND_ACCESSIBILITY_SERVICE untuk mendeteksi aplikasi yang dijalankan.
▸ REQUEST_IGNORE_BATTERY_OPTIMIZATIONS untuk memastikan bahwa aplikasi ini akan terus berjalan di latar belakang.
▸ RECEIVE_BOOT_COMPLETED untuk secara otomatis dimulai pada saat  boot jika diaktifkan.
▸ PACKAGE_USAGE_STATS untuk mendapatkan info mengenai aplikasi yang sedang berjalan.

<b>Dapatkan</b>
Anda bisa mengunduhnya secara gratis di F-Droid[3]

<b>Terjemahan</b>

Terjemahan selalu diterima! :)

Aplikasi ini tersedia untuk diterjemahkan sebagai dua projek di Transifex[4]

[1] https://www.gnu.org/licenses/gpl-3.0.en.html
[2] https://gitlab.com/juanitobananas/auto-auto-rotate
[3] https://f-droid.org/packages/com.jarsilio.android.autoautorotate/
[4] https://www.transifex.com/juanitobananas/auto-auto-rotate and https://www.transifex.com/juanitobananas/libcommon