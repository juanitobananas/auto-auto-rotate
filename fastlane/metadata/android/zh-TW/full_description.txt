<i>Auto Auto-Rotate</i> 一個非常簡單的應用程式，存儲Android的自動旋轉設置每個應用程式。

您是否通常禁用自動旋轉並僅對某些應用啟用它？喜歡你的圖庫，還是你的視頻應用？你往往會忘記以後關掉它嗎？麻煩你了嗎？

然後 <i>Auto Auto-Rotate</i> 是完美的應用程式給你！

<i>Auto Auto-Rotate</i> 開源的，在 GPLv3 許可證[1] 下發佈。如果您喜歡，請查看代碼{2}。

<b>所需的 Android 許可權</b>

• WRITE_SETTINGS打開和關閉 Android 的自動旋轉。
• BIND_ACCESSIBILITY_SERVICE檢測應用啟動。
• REQUEST_IGNORE_BATTERY_OPTIMIZATIONS以確保它在後台繼續運行。
• RECEIVE_BOOT_COMPLETED啟動時自動啟動。
▸ PACKAGE_USAGE_STATS獲取當前正在運行的應用。

<b>得到它</b>
您可以在F-Droid免費下載[3]

<b>翻譯</b>

翻譯總是受歡迎的！:)

該應用程式可用於翻譯作為兩個專案的Transifex[4]

{1} https://www.gnu.org/licenses/gpl-3.0.en.html
{2} https://gitlab.com/juanitobananas/auto-auto-rotate
{3} https://f-droid.org/packages/com.jarsilio.android.autoautorotate/
[4] https://www.transifex.com/juanitobananas/auto-auto-rotate https://www.transifex.com/juanitobananas/libcommon